// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'loading_status.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LoadingStatus {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Failure? failure) loading,
    required TResult Function() loaded,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        refreshing,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        fetchMore,
    required TResult Function() noMoreData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Failure? failure)? loading,
    TResult? Function()? loaded,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult? Function()? noMoreData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Failure? failure)? loading,
    TResult Function()? loaded,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult Function()? noMoreData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(Loaded value) loaded,
    required TResult Function(RefreshingStatus value) refreshing,
    required TResult Function(FetchingMoreStatus value) fetchMore,
    required TResult Function(NoMoreDataStatus value) noMoreData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(Loaded value)? loaded,
    TResult? Function(RefreshingStatus value)? refreshing,
    TResult? Function(FetchingMoreStatus value)? fetchMore,
    TResult? Function(NoMoreDataStatus value)? noMoreData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(Loaded value)? loaded,
    TResult Function(RefreshingStatus value)? refreshing,
    TResult Function(FetchingMoreStatus value)? fetchMore,
    TResult Function(NoMoreDataStatus value)? noMoreData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoadingStatusCopyWith<$Res> {
  factory $LoadingStatusCopyWith(
          LoadingStatus value, $Res Function(LoadingStatus) then) =
      _$LoadingStatusCopyWithImpl<$Res, LoadingStatus>;
}

/// @nodoc
class _$LoadingStatusCopyWithImpl<$Res, $Val extends LoadingStatus>
    implements $LoadingStatusCopyWith<$Res> {
  _$LoadingStatusCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadingCopyWith<$Res> {
  factory _$$LoadingCopyWith(_$Loading value, $Res Function(_$Loading) then) =
      __$$LoadingCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure? failure});

  $FailureCopyWith<$Res>? get failure;
}

/// @nodoc
class __$$LoadingCopyWithImpl<$Res>
    extends _$LoadingStatusCopyWithImpl<$Res, _$Loading>
    implements _$$LoadingCopyWith<$Res> {
  __$$LoadingCopyWithImpl(_$Loading _value, $Res Function(_$Loading) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_$Loading(
      failure: freezed == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $FailureCopyWith<$Res>? get failure {
    if (_value.failure == null) {
      return null;
    }

    return $FailureCopyWith<$Res>(_value.failure!, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc

class _$Loading extends Loading {
  const _$Loading({this.failure}) : super._();

  @override
  final Failure? failure;

  @override
  String toString() {
    return 'LoadingStatus.loading(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Loading &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoadingCopyWith<_$Loading> get copyWith =>
      __$$LoadingCopyWithImpl<_$Loading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Failure? failure) loading,
    required TResult Function() loaded,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        refreshing,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        fetchMore,
    required TResult Function() noMoreData,
  }) {
    return loading(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Failure? failure)? loading,
    TResult? Function()? loaded,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult? Function()? noMoreData,
  }) {
    return loading?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Failure? failure)? loading,
    TResult Function()? loaded,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult Function()? noMoreData,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(Loaded value) loaded,
    required TResult Function(RefreshingStatus value) refreshing,
    required TResult Function(FetchingMoreStatus value) fetchMore,
    required TResult Function(NoMoreDataStatus value) noMoreData,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(Loaded value)? loaded,
    TResult? Function(RefreshingStatus value)? refreshing,
    TResult? Function(FetchingMoreStatus value)? fetchMore,
    TResult? Function(NoMoreDataStatus value)? noMoreData,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(Loaded value)? loaded,
    TResult Function(RefreshingStatus value)? refreshing,
    TResult Function(FetchingMoreStatus value)? fetchMore,
    TResult Function(NoMoreDataStatus value)? noMoreData,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading extends LoadingStatus {
  const factory Loading({final Failure? failure}) = _$Loading;
  const Loading._() : super._();

  Failure? get failure;
  @JsonKey(ignore: true)
  _$$LoadingCopyWith<_$Loading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoadedCopyWith<$Res> {
  factory _$$LoadedCopyWith(_$Loaded value, $Res Function(_$Loaded) then) =
      __$$LoadedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadedCopyWithImpl<$Res>
    extends _$LoadingStatusCopyWithImpl<$Res, _$Loaded>
    implements _$$LoadedCopyWith<$Res> {
  __$$LoadedCopyWithImpl(_$Loaded _value, $Res Function(_$Loaded) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Loaded extends Loaded {
  const _$Loaded() : super._();

  @override
  String toString() {
    return 'LoadingStatus.loaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Loaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Failure? failure) loading,
    required TResult Function() loaded,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        refreshing,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        fetchMore,
    required TResult Function() noMoreData,
  }) {
    return loaded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Failure? failure)? loading,
    TResult? Function()? loaded,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult? Function()? noMoreData,
  }) {
    return loaded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Failure? failure)? loading,
    TResult Function()? loaded,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult Function()? noMoreData,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(Loaded value) loaded,
    required TResult Function(RefreshingStatus value) refreshing,
    required TResult Function(FetchingMoreStatus value) fetchMore,
    required TResult Function(NoMoreDataStatus value) noMoreData,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(Loaded value)? loaded,
    TResult? Function(RefreshingStatus value)? refreshing,
    TResult? Function(FetchingMoreStatus value)? fetchMore,
    TResult? Function(NoMoreDataStatus value)? noMoreData,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(Loaded value)? loaded,
    TResult Function(RefreshingStatus value)? refreshing,
    TResult Function(FetchingMoreStatus value)? fetchMore,
    TResult Function(NoMoreDataStatus value)? noMoreData,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class Loaded extends LoadingStatus {
  const factory Loaded() = _$Loaded;
  const Loaded._() : super._();
}

/// @nodoc
abstract class _$$RefreshingStatusCopyWith<$Res> {
  factory _$$RefreshingStatusCopyWith(
          _$RefreshingStatus value, $Res Function(_$RefreshingStatus) then) =
      __$$RefreshingStatusCopyWithImpl<$Res>;
  @useResult
  $Res call({Completer<dynamic>? completer, Failure? failure});

  $FailureCopyWith<$Res>? get failure;
}

/// @nodoc
class __$$RefreshingStatusCopyWithImpl<$Res>
    extends _$LoadingStatusCopyWithImpl<$Res, _$RefreshingStatus>
    implements _$$RefreshingStatusCopyWith<$Res> {
  __$$RefreshingStatusCopyWithImpl(
      _$RefreshingStatus _value, $Res Function(_$RefreshingStatus) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = freezed,
    Object? failure = freezed,
  }) {
    return _then(_$RefreshingStatus(
      freezed == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<dynamic>?,
      failure: freezed == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $FailureCopyWith<$Res>? get failure {
    if (_value.failure == null) {
      return null;
    }

    return $FailureCopyWith<$Res>(_value.failure!, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc

class _$RefreshingStatus extends RefreshingStatus {
  const _$RefreshingStatus(this.completer, {this.failure}) : super._();

  @override
  final Completer<dynamic>? completer;
  @override
  final Failure? failure;

  @override
  String toString() {
    return 'LoadingStatus.refreshing(completer: $completer, failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RefreshingStatus &&
            (identical(other.completer, completer) ||
                other.completer == completer) &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, completer, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RefreshingStatusCopyWith<_$RefreshingStatus> get copyWith =>
      __$$RefreshingStatusCopyWithImpl<_$RefreshingStatus>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Failure? failure) loading,
    required TResult Function() loaded,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        refreshing,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        fetchMore,
    required TResult Function() noMoreData,
  }) {
    return refreshing(completer, failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Failure? failure)? loading,
    TResult? Function()? loaded,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult? Function()? noMoreData,
  }) {
    return refreshing?.call(completer, failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Failure? failure)? loading,
    TResult Function()? loaded,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult Function()? noMoreData,
    required TResult orElse(),
  }) {
    if (refreshing != null) {
      return refreshing(completer, failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(Loaded value) loaded,
    required TResult Function(RefreshingStatus value) refreshing,
    required TResult Function(FetchingMoreStatus value) fetchMore,
    required TResult Function(NoMoreDataStatus value) noMoreData,
  }) {
    return refreshing(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(Loaded value)? loaded,
    TResult? Function(RefreshingStatus value)? refreshing,
    TResult? Function(FetchingMoreStatus value)? fetchMore,
    TResult? Function(NoMoreDataStatus value)? noMoreData,
  }) {
    return refreshing?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(Loaded value)? loaded,
    TResult Function(RefreshingStatus value)? refreshing,
    TResult Function(FetchingMoreStatus value)? fetchMore,
    TResult Function(NoMoreDataStatus value)? noMoreData,
    required TResult orElse(),
  }) {
    if (refreshing != null) {
      return refreshing(this);
    }
    return orElse();
  }
}

abstract class RefreshingStatus extends LoadingStatus {
  const factory RefreshingStatus(final Completer<dynamic>? completer,
      {final Failure? failure}) = _$RefreshingStatus;
  const RefreshingStatus._() : super._();

  Completer<dynamic>? get completer;
  Failure? get failure;
  @JsonKey(ignore: true)
  _$$RefreshingStatusCopyWith<_$RefreshingStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FetchingMoreStatusCopyWith<$Res> {
  factory _$$FetchingMoreStatusCopyWith(_$FetchingMoreStatus value,
          $Res Function(_$FetchingMoreStatus) then) =
      __$$FetchingMoreStatusCopyWithImpl<$Res>;
  @useResult
  $Res call({Completer<dynamic>? completer, Failure? failure});

  $FailureCopyWith<$Res>? get failure;
}

/// @nodoc
class __$$FetchingMoreStatusCopyWithImpl<$Res>
    extends _$LoadingStatusCopyWithImpl<$Res, _$FetchingMoreStatus>
    implements _$$FetchingMoreStatusCopyWith<$Res> {
  __$$FetchingMoreStatusCopyWithImpl(
      _$FetchingMoreStatus _value, $Res Function(_$FetchingMoreStatus) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? completer = freezed,
    Object? failure = freezed,
  }) {
    return _then(_$FetchingMoreStatus(
      freezed == completer
          ? _value.completer
          : completer // ignore: cast_nullable_to_non_nullable
              as Completer<dynamic>?,
      failure: freezed == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $FailureCopyWith<$Res>? get failure {
    if (_value.failure == null) {
      return null;
    }

    return $FailureCopyWith<$Res>(_value.failure!, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc

class _$FetchingMoreStatus extends FetchingMoreStatus {
  const _$FetchingMoreStatus(this.completer, {this.failure}) : super._();

  @override
  final Completer<dynamic>? completer;
  @override
  final Failure? failure;

  @override
  String toString() {
    return 'LoadingStatus.fetchMore(completer: $completer, failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchingMoreStatus &&
            (identical(other.completer, completer) ||
                other.completer == completer) &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, completer, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchingMoreStatusCopyWith<_$FetchingMoreStatus> get copyWith =>
      __$$FetchingMoreStatusCopyWithImpl<_$FetchingMoreStatus>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Failure? failure) loading,
    required TResult Function() loaded,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        refreshing,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        fetchMore,
    required TResult Function() noMoreData,
  }) {
    return fetchMore(completer, failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Failure? failure)? loading,
    TResult? Function()? loaded,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult? Function()? noMoreData,
  }) {
    return fetchMore?.call(completer, failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Failure? failure)? loading,
    TResult Function()? loaded,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult Function()? noMoreData,
    required TResult orElse(),
  }) {
    if (fetchMore != null) {
      return fetchMore(completer, failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(Loaded value) loaded,
    required TResult Function(RefreshingStatus value) refreshing,
    required TResult Function(FetchingMoreStatus value) fetchMore,
    required TResult Function(NoMoreDataStatus value) noMoreData,
  }) {
    return fetchMore(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(Loaded value)? loaded,
    TResult? Function(RefreshingStatus value)? refreshing,
    TResult? Function(FetchingMoreStatus value)? fetchMore,
    TResult? Function(NoMoreDataStatus value)? noMoreData,
  }) {
    return fetchMore?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(Loaded value)? loaded,
    TResult Function(RefreshingStatus value)? refreshing,
    TResult Function(FetchingMoreStatus value)? fetchMore,
    TResult Function(NoMoreDataStatus value)? noMoreData,
    required TResult orElse(),
  }) {
    if (fetchMore != null) {
      return fetchMore(this);
    }
    return orElse();
  }
}

abstract class FetchingMoreStatus extends LoadingStatus {
  const factory FetchingMoreStatus(final Completer<dynamic>? completer,
      {final Failure? failure}) = _$FetchingMoreStatus;
  const FetchingMoreStatus._() : super._();

  Completer<dynamic>? get completer;
  Failure? get failure;
  @JsonKey(ignore: true)
  _$$FetchingMoreStatusCopyWith<_$FetchingMoreStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NoMoreDataStatusCopyWith<$Res> {
  factory _$$NoMoreDataStatusCopyWith(
          _$NoMoreDataStatus value, $Res Function(_$NoMoreDataStatus) then) =
      __$$NoMoreDataStatusCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoMoreDataStatusCopyWithImpl<$Res>
    extends _$LoadingStatusCopyWithImpl<$Res, _$NoMoreDataStatus>
    implements _$$NoMoreDataStatusCopyWith<$Res> {
  __$$NoMoreDataStatusCopyWithImpl(
      _$NoMoreDataStatus _value, $Res Function(_$NoMoreDataStatus) _then)
      : super(_value, _then);
}

/// @nodoc

class _$NoMoreDataStatus extends NoMoreDataStatus {
  const _$NoMoreDataStatus() : super._();

  @override
  String toString() {
    return 'LoadingStatus.noMoreData()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NoMoreDataStatus);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Failure? failure) loading,
    required TResult Function() loaded,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        refreshing,
    required TResult Function(Completer<dynamic>? completer, Failure? failure)
        fetchMore,
    required TResult Function() noMoreData,
  }) {
    return noMoreData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Failure? failure)? loading,
    TResult? Function()? loaded,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult? Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult? Function()? noMoreData,
  }) {
    return noMoreData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Failure? failure)? loading,
    TResult Function()? loaded,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        refreshing,
    TResult Function(Completer<dynamic>? completer, Failure? failure)?
        fetchMore,
    TResult Function()? noMoreData,
    required TResult orElse(),
  }) {
    if (noMoreData != null) {
      return noMoreData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(Loaded value) loaded,
    required TResult Function(RefreshingStatus value) refreshing,
    required TResult Function(FetchingMoreStatus value) fetchMore,
    required TResult Function(NoMoreDataStatus value) noMoreData,
  }) {
    return noMoreData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(Loaded value)? loaded,
    TResult? Function(RefreshingStatus value)? refreshing,
    TResult? Function(FetchingMoreStatus value)? fetchMore,
    TResult? Function(NoMoreDataStatus value)? noMoreData,
  }) {
    return noMoreData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(Loaded value)? loaded,
    TResult Function(RefreshingStatus value)? refreshing,
    TResult Function(FetchingMoreStatus value)? fetchMore,
    TResult Function(NoMoreDataStatus value)? noMoreData,
    required TResult orElse(),
  }) {
    if (noMoreData != null) {
      return noMoreData(this);
    }
    return orElse();
  }
}

abstract class NoMoreDataStatus extends LoadingStatus {
  const factory NoMoreDataStatus() = _$NoMoreDataStatus;
  const NoMoreDataStatus._() : super._();
}
