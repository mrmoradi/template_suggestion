import 'dart:async';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:reza_template_suggestion/feature/shared/models/failure.dart';

part 'loading_status.freezed.dart';

abstract class Watchable<T> {
  LoadingStatus get loadingStatus;
  T get data;
}

@freezed
class LoadingStatus with _$LoadingStatus {
  const factory LoadingStatus.loading({Failure? failure}) = Loading;
  const factory LoadingStatus.loaded() = Loaded;
  const factory LoadingStatus.refreshing(
    Completer? completer, {
    Failure? failure,
  }) = RefreshingStatus;
  const factory LoadingStatus.fetchingMore(
    Completer? completer, {
    Failure? failure,
  }) = FetchingMoreStatus;

  const factory LoadingStatus.noMoreData() = NoMoreDataStatus;

  const LoadingStatus._();

  Failure? get failure => when(
        loading: (f) => f,
        loaded: () => null,
        refreshing: (c, f) => f,
        fetchMore: (c, f) => f,
        noMoreData: () => null,
      );

  bool get hasFailure => failure != null;
}
