import 'dart:async';

import 'package:riverpod/riverpod.dart';

import 'failure.dart';
import 'loading_status.dart';

mixin Watching<T, S extends Watchable<T>> on StateNotifier<S> {
  StreamSubscription<T>? subscription;

  LoadingStatus loadingStatus({
    Failure? failure,
    int? length,
    bool isRandom = false,
  }) {
    return state.loadingStatus.when(
        loading: (_) {
          if (failure != null) {
            return LoadingStatus.loading(failure: failure);
          }
          return const LoadingStatus.loaded();
        },
        loaded: () {
          return const LoadingStatus.loaded();
        },
        refreshing: (completer, _) {
          completer?.complete();
          // Failed
          if (failure != null) {
            return LoadingStatus.refreshing(null, failure: failure);
          }
          return const LoadingStatus.loaded();
        },
        fetchMore: (completer, _) {
          completer?.complete();
          // Failed
          if (failure != null) {
            return LoadingStatus.fetchingMore(null, failure: failure);
          }
          if (state.data is List) {
            if (isRandom || (state.data as List).length == length) {
              return const LoadingStatus.noMoreData();
            }
          }
          return const LoadingStatus.loaded();
        },
        noMoreData: () => const LoadingStatus.noMoreData());
  }

  StreamSubscription<T> listen();

  void watched() {
    subscription?.cancel();
    subscription = listen();
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }
}
