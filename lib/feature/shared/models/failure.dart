import 'package:freezed_annotation/freezed_annotation.dart';

part 'failure.freezed.dart';

@freezed
class Failure with _$Failure {
  const factory Failure.noConnection() = _NoConnection;
  const factory Failure.general() = _GeneralFailure;
  const Failure._();

  String get errorMessage => when(
      noConnection: () => 'There is no connection',
      general: () => 'some thing general happend');
}
