import 'dart:async';
import 'package:reza_template_suggestion/feature/profile/model/profile.dart';
import 'package:reza_template_suggestion/feature/profile/model/profile_state.dart';
import 'package:reza_template_suggestion/feature/profile/profile_repo.dart';
import 'package:reza_template_suggestion/feature/shared/models/loading_status.dart';
import 'package:riverpod/riverpod.dart';

class ProfileStateNotifier extends StateNotifier<ProfileState> {
  final ProfileRepo _profileRepo;
  ProfileStateNotifier(this._profileRepo) : super(ProfileState.initial);

  Future<void> fetchOwnProfile() async {
    return await _profileRepo.fetchOwnProfile().then((profile) {
      state = state.copyWith(
          profile: Profile(
        id: profile.userId,
        userName: profile.userId,
        photoUrl: profile.avatarUrl.toString(),
        name: profile.displayName ?? 'something',
      ),
      loadingStatus: const LoadingStatus.loaded());
    });
  }
}
