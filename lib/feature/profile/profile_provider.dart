import 'package:matrix/matrix.dart';
import 'package:reza_template_suggestion/feature/profile/model/profile_state.dart';
import 'package:reza_template_suggestion/feature/profile/profile_repo.dart';
import 'package:reza_template_suggestion/feature/profile/profile_state_notifier.dart';
import 'package:riverpod/riverpod.dart';

final profileRepoProvider = Provider<ProfileRepo>((ref) {
  // It could also have it's own provider.
  final client = Client('HappyChat');
  final profileRepo = ProfileRepo(client);

  return profileRepo;
});

final profileStateNotifierProvider =
    StateNotifierProvider.autoDispose<ProfileStateNotifier,ProfileState>((ref) {
  final profileRepo = ref.watch(profileRepoProvider);
  return ProfileStateNotifier(profileRepo);
});
