import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:reza_template_suggestion/feature/shared/models/loading_status.dart';
import 'profile.dart';

part 'profile_state.freezed.dart';

@freezed
class ProfileState with _$ProfileState implements Watchable<Profile> {
  const factory ProfileState({
    required Profile profile,
    @Default(LoadingStatus.loading()) LoadingStatus loadingStatus,
  }) = _ProfileState;

  const ProfileState._();

  static const initial = ProfileState(profile: Profile.initial);

  @override
  Profile get data => profile;
}
