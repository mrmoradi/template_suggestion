import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile.freezed.dart';

@freezed
class Profile with _$Profile {
  const factory Profile({
    required String id,
    required String userName,
    required String? photoUrl,
    required String name,
  }) = _Profile;

  const Profile._();
  static const initial = Profile(
    id: '',
    userName: '',
    photoUrl: '',
    name: '',
  );
}
