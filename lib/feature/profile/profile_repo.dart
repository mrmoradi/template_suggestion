import 'package:matrix/matrix.dart';

class ProfileRepo {
  final Client client;

  ProfileRepo(this.client);

  Future<Profile> fetchOwnProfile() async {
    return client.fetchOwnProfile();
  }
 // What ever function related to profile
}
