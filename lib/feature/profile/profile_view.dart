import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reza_template_suggestion/feature/profile/profile_provider.dart';
import 'package:reza_template_suggestion/feature/shared/models/loading_status.dart';

class ProfileView extends ConsumerWidget {
  const ProfileView({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // Just to demonstrate it.
    // final stateNotifier = ref.watch(profileStateNotifierProvider.notifier);
    final loadingStatus = ref.watch(profileStateNotifierProvider).loadingStatus;
    final profile = ref.watch(profileStateNotifierProvider).profile;

    return loadingStatus is Loading
        ? const CircularProgressIndicator()
        : ListTile(
            leading: Text(profile.photoUrl ?? 'something'),
            title: Text(profile.name),
            subtitle: Text(profile.userName),
          );
  }
}
